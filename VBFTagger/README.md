# VBF Tagger

This package provides an implementation of the inference of the VBF-RNN tagger in analyses frameworks.

Author: Antonio Giannini

email: antonio.giannini@cern.ch

Contact me for any technical issues or new functionalities to add;
any for feedback about documentation and configuration is welcome.

This repository is the main one,
it is added (MR ongoing) to the EasyJet fw as sub-module. 

## Overview of the tagger

The tagger is based on the R&D done, applied and pushished for the full run-2 VV semi-leptonic high mass search: https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/HDBS-2018-10/.

The baseline model is based on a RNN model trained using small-R jets 4-momentum sequences to distinguish between VBF vs ggF production mechanism for high mass higgs.

Some useful links:

- ATLAS paper: [Eur. Phys. J. C 80 (2020) 1165](https://link.springer.com/article/10.1140/epjc/s10052-020-08554-y)
- pheno paper: [VBF Event Classification with Recurrent Neural Networks at ATLAS’s LHC Experiment](https://www.mdpi.com/2076-3417/13/5/3282)
- HDBS workshop: [jun '19](https://indico.cern.ch/event/801402/timetable/?view=standard#80-resonance-searches-in-vv-se)
- ML workshop: [nov '19](https://indico.cern.ch/event/844092/timetable/#20-ml-for-hdbs-dbl-vv-semi-lep)

## Get the package

You can download the package from git:

```
git clone ssh://git@gitlab.cern.ch:7999/vbftagger/vbftagger.git VBFTagger
```

for now you can seat on the master branch, likely, tags will be released in future.

You can add it to your analysis framework as a sub-module:
```
git submodule add -- "ssh://git@gitlab.cern.ch:7999/angianni/vbftagger.git" VBFTagger
```

it will be linked as a hashtag to your main repository and, after it, you can download it using the --recursive option from your framework.

## Standalone compile
You can setup the package in a standalone way.
This works for both AthAnalysis and AnalysisBase.

```
mkdir source
cd source/
git clone ssh://git@gitlab.cern.ch:7999/angianni/vbftagger.git VBFTagger
mkdir build
cd build/
setupATLAS
asetup AthAnalysis, 25.2.17
cmake ../source/
make -j88
```

Remember to place a project CMakeList in the source directory before cmake-ing.

## Tool usage

The usual ATHENA tool usage is supported.

Initialisation:
```
asg::StandaloneToolHandle<VBFTagger> m_Tagger; //!
m_Tagger.setTypeAndName("VBFTagger/MyTagger");
if(verbose) ANA_CHECK( m_Tagger.setProperty("OutputLevel", MSG::DEBUG) );

ANA_CHECK( m_Tagger.setProperty( "modelTag", "VBFRNNv0") );
ANA_CHECK( m_Tagger.setProperty( "IsDebug", verbose) );
ANA_CHECK( m_Tagger.setProperty( "pTCut", 30.e3) );

ANA_CHECK( m_Tagger.retrieve() );
```

Execute event per event:

```
ANA_CHECK( m_Tagger -> SelectInputJets(*shallowJets));
ANA_CHECK( m_Tagger -> execute() );

nRNNJets = m_Tagger -> GetNRNNJets();
RNNScore = m_Tagger -> GetRNNScore();
```

If there are "signal" jets in your topology, i.e. jets coming from the recostruction of hadronic decays of W/Z/h, 
you should remove those candidates from the jets used as input to the tagger;
you can add this configuration in the initialisation of the tool:

```
ANA_CHECK( m_Tagger.setProperty( "PreProcJetsMethod", "RemoveFirst2Jets") );
```

few options are supported: 
```
RemoveFirst2Jets, RemoveFirst4Jets, RemoveWJets, RemoveZJets, RemovehJets.
```
the first two will remove the first 2 or the first 4 leading pT jets,
while the other 3 will remove the two jets with invariant mass closer to the nominal
mass value of SM W, Z, h bosons.

Instead of this configuration, you can directly pass the set of jets to use from your analysis code
and remove the unwanted ones at that level.
This functionality is supported only if you are using the VBFTagger tool and not the VBFTaggerAlg.

In particular, when using the VBFTagger tool, the jets can be passed using one of these method:
```
virtual StatusCode SelectInputJets(const xAOD::JetContainer& jets);
virtual StatusCode SelectInputJets(std::vector<std::unique_ptr<const xAOD::Jet*>>& jets);
```

the first one is more general, but it can be more annoying to prepare at the analysi framework level, 
so, you can use the latter one.


## Algorithm usage
An algorithm based usage is also supported. 
This is has been designed following the EasyJet framework standard, other functionalities can be supported.

Specifically on the EasyJet framework both the SG::ReadHandleKey and the CP::SysReadHandle are supported to handle the jets collections.
Examples for both are below.

Example in the EasyJet framework for nominal only (SG::ReadHandleKey):
```python
vbftagger = CompFactory.VBFTagger("VBFTaggerTool", modelTag = "VBFRNNv0")
cfg.addEventAlgo(
    CompFactory.VBFTaggerAlg(
        "VBFTaggerAlg_" + btag_wp,
        VBFTagger = vbftagger,
        containerAllJetsKey = smalljetkey,
        containerSigJetsKey = "resolvedAnalysisJets_" + btag_wp,
        pTCut = 30.e+3,
        nMaxJets = 2,
        DecTag = "_" + btag_wp
    )
)
```

Example in the EasyJet framework for systematics (CP::SysReadHandle):
```python
vbftagger = CompFactory.VBFTagger("VBFTaggerTool", modelTag = "VBFRNNv0")
cfg.addEventAlgo(
    CompFactory.VBFTaggerAlgSys(
        "VBFTaggerAlg",
        VBFTagger = vbftagger,
        containerAllJetsKey = "vbshiggsAnalysisJets_%SYS%",
        containerSigJetsKey = "vbshiggsAnalysisSignalJets_%SYS%",
        pTCut = 30.e+3,
        nMaxJets = 2,
        DecTag = ""
    )
)
```

If there are "signal" jets in your topology, i.e. jets coming from the recostruction of hadronic decays of W/Z/h, 
you should remove those candidates from the jets used as input to the tagger.
For the algorithm usage you can pass the main small-R jets collection and the collections
of signal small-R and/or signal large-R jets in your analysis and the algorithm will remove the overlap for you.
The configurations are:

```
containerSigJetsKey = "resolvedAnalysisJets_" + btag_wp,
containerSigLargeRJetsKey = "boostedAnalysisJets_" + btag_wp,
```
