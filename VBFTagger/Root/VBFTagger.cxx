///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// VBFTagger.cxx
// Source file for class VBFTagger
// Author: Antonio Giannini
// Email : antonio.giannini@cern.ch
///////////////////////////////////////////////////////////////////

// VBFTagger includes
#include "VBFTagger/VBFTagger.h"
#include "PathResolver/PathResolver.h"

#include "TLorentzVector.h"
#include "xAODJet/Jet.h"
#include "AthContainers/AuxElement.h"

#include "TString.h"
#include <TSystem.h>

VBFTagger::VBFTagger(const std::string &name)
    : asg::AsgTool(name),
    m_IsDebug (false),
    m_IsPureRNN (true),
    m_nMaxJets (2),
    m_nRNNJets (0),
    m_mW (80.38),
    m_mZ (91.19),
    m_mh (125.25),
    m_pTCut (30.e3)
{
    declareProperty("IsDebug", m_IsDebug);
    declareProperty("nMaxJets", m_nMaxJets);
    declareProperty("PreProcJetsMethod", m_PreProcJetsMethod);
    declareProperty("pTCut", m_pTCut);
    declareProperty("modelTag", m_modelTag);
}

VBFTagger::~VBFTagger() {}

StatusCode VBFTagger::initialize()
{
  ANA_MSG_INFO("VBFTagger::initialise ... " << name() << " ... ");

  // read feature scaling parameters
  ATH_CHECK(ReadScalingValues());

  // read model
  ATH_CHECK(ReadRNNModel());

  return StatusCode::SUCCESS;
}

StatusCode VBFTagger::ReadRNNModel(){

  if(m_IsDebug) 
    ANA_MSG_INFO(" --- ReadRNNModel ---");

  // expand path
  std::string modelPath = PathResolverFindCalibFile("VBFTagger/" + m_modelTag + "/neural_net.json");
  ANA_MSG_INFO("   -  " << modelPath);

  // get your saved JSON file as an std::istream object
  std::ifstream input(modelPath);

  // put some input file check!
  if(m_IsDebug) 
    ANA_MSG_INFO(" --- Loading RNNModel from path:   " << modelPath);
  
  // build the graph
  m_config = lwt::parse_json_graph(input);
  assert(m_config.outputs.size() > 0);

  return StatusCode::SUCCESS; 

}

StatusCode VBFTagger::ReadScalingValues(){

  if(m_IsDebug) 
    ANA_MSG_INFO(" --- ReadScalingValues ---");

  std::string name_file_in = PathResolverFindCalibFile("VBFTagger/" + m_modelTag + "/FeaturesScaling.dat");
  std::ifstream file_in( name_file_in );
  if(m_IsDebug) 
    ANA_MSG_INFO("   - Loading RNNModel FeaturesScaling from path:   " << name_file_in);

  int nlines = 0;
  std::string line;
  TString entry;
  char next;

  std::vector<std::string> variables = {"pT", "eta", "phi", "E", "nTracks"};

  // loop over file and read parameters
  if( file_in.is_open() ){

    while( getline(file_in, line) ){
      nlines++;
      //std::cout << nlines << std::endl;
    }
    file_in.close();
    file_in.open(name_file_in);
    for(int l=0; l<nlines; l++){
      file_in >> entry ;
      //std::cout << entry << std::endl;
      if(entry.Contains("#")) continue;

      for(auto var : variables){
        if(entry.Contains( var + "_mean")) file_in >> m_fscaler_mean[var];
        if(entry.Contains( var + "_std")) file_in >> m_fscaler_std[var];
      };

      file_in >> entry;
      // check end line format
      if(!entry.Contains("$")){
        ANA_MSG_INFO("ERROR! Wrong input format");
        //exit(1);
      }

    }
  }

  // print out
  for(auto var : variables){
    ANA_MSG_INFO("   - " << var << ": " << m_fscaler_mean[var] << "   " << m_fscaler_std[var]);
  }

  return StatusCode::SUCCESS; 

}

StatusCode VBFTagger::execute() { 

  ANA_MSG_INFO("VBFTagger::execute");

  ATH_CHECK( LoadRNN() );
  ATH_MSG_INFO(" --- RNN score: " << m_RNNScore);

  return StatusCode::SUCCESS; 
}

StatusCode VBFTagger::LoadRNN(){

  if(m_IsDebug) 
    ANA_MSG_INFO(" --- LoadRNN --- ");

  lwt::LightweightGraph tagger(m_config, m_config.outputs.begin()->first);

  lwt::LightweightGraph::SeqNodeMap seq = get_sequences(m_config.input_sequences);
  lwt::LightweightGraph::NodeMap in_nodes;

  // get the inputs part of the dense layers
  if(!m_IsPureRNN) in_nodes = get_flat_inputs(m_config.inputs);

  // Loop over the output names and compute the output for each
  for (const auto& output: m_config.outputs) {
    auto out_vals = tagger.compute(in_nodes, seq, output.first);
    if(m_IsDebug){
      std::cout << output.first << ":" << std::endl;
      for (const auto& out: out_vals) {
      	std::cout << out.first << " " << out.second << std::endl;
      }
    }
    
    m_RNNScore = out_vals["out_0"];
    //if(IsDebug) std::cout << RNNScore << "   " << Jet1_pt << "    " << Jet1_eta << std::endl;
    //if(IsDebug) std::cout << RNNScore << "   " << FatJet.Pt() << "    " << FatJetnTracks << std::endl;
  }

  return StatusCode::SUCCESS; 
}

lwt::LightweightGraph::NodeMap VBFTagger::get_flat_inputs(const std::vector<lwt::InputNodeConfig>& config) {

  std::map<std::string, std::map<std::string, double> > in_nodes;

  // loop over inputs
  for (const auto& input: config) {
    const size_t total_inputs = input.variables.size();
    if(m_IsDebug) std::cout << "Total Inputs   " << total_inputs << std::endl;
    std::map<std::string, double> in_vals;

    SetFlatInputs(in_vals); // you can imagine to extend the method in future for different model scnarios ---
    in_nodes[input.name] = in_vals;
  }

  return in_nodes;
}

void VBFTagger::SetFlatInputs(std::map<std::string, double> &in_vals){

  // ToBeImplemented according to the support of the tool in the future

  /*
  in_vals["PtFatJet"]  = FatJet.Pt() * 1.e-3 ;
  in_vals["EtaFatJet"] = FatJet.Eta() ;
  in_vals["PhiFatJet"] = FatJet.Phi() ;
  in_vals["EFatJet"]   = FatJet.E()  * 1.e-3 ;

  if(UseJSSVars){
    in_vals["D2FatJet"] = FatJetD2 ;
    in_vals["nTracksFatJet"] = FatJetnTracks ;
  }
  */

  return;
}

lwt::LightweightGraph::SeqNodeMap VBFTagger::get_sequences(const std::vector<lwt::InputNodeConfig>& config) {
  lwt::LightweightGraph::SeqNodeMap nodes;
  for (const auto& input: config) {
    // see the `test_utilities` header for this function.
    nodes[input.name] = get_JetVars(input.variables, 6);
  }
  return nodes;
}

lwt::VectorMap VBFTagger::get_JetVars(const std::vector<lwt::Input>& inputs, size_t n_patterns) {
  lwt::VectorMap out, out_mask;
  // ramp through the input multiplier
  const size_t total_inputs = inputs.size(); // nVariables = [pT, eta, phi, E]
  if( m_IsDebug ) std::cout << "nInputs   " << total_inputs << std::endl;
  for (size_t nnn = 0; nnn < total_inputs; nnn++) {
    const auto& input = inputs.at(nnn);
    out[input.name] = {};
    out_mask[input.name] = {};

    double mean = 0., std = -1.;

    if( input.name=="variable_0" ){
      out.at(input.name) = m_jets_pT;
      mean = m_fscaler_mean["pT"];
      std = m_fscaler_std["pT"];
    }
    if( input.name=="variable_1" ){
      out.at(input.name) = m_jets_eta;
      mean = m_fscaler_mean["eta"];
      std = m_fscaler_std["eta"];
    }
    if( input.name=="variable_2" ){
      out.at(input.name) = m_jets_phi;
      mean = m_fscaler_mean["phi"];
      std = m_fscaler_std["phi"];
    }
    if( input.name=="variable_3" ){
      out.at(input.name) = m_jets_E;
      mean = m_fscaler_mean["E"];
      std = m_fscaler_std["E"];
    }
    if( input.name=="variable_4" && m_UseNTracks ){
      out.at(input.name) = m_jets_nTracks;
      mean = m_fscaler_mean["nTracks"];
      std = m_fscaler_std["nTracks"];
    }

    for(int i=0; i<out.at(input.name).size(); i++){
      if(m_IsDebug){
        std::cout << i << std::endl;
        std::cout << out.at(input.name).at(i) << std::endl;
      }
      if(out.at(input.name).at(i) < -33.) continue;
      out.at(input.name).at(i) = ( out.at(input.name).at(i) - mean ) / std;
      if(m_IsDebug) std::cout<< out.at(input.name).at(i) << std::endl;

      out_mask.at(input.name).push_back( out.at(input.name).at(i) );
    }

  }
  return out_mask;
}

StatusCode VBFTagger::SetInputSequences(std::vector<double> pT, std::vector<double> eta, std::vector<double> phi, std::vector<double> E){

  ATH_MSG_INFO(" --- SetInputSequences --- ");

  // set the sequences
  m_jets_pT  = pT; 
  m_jets_eta = eta; 
  m_jets_phi = phi; 
  m_jets_E   = E;

  return StatusCode::SUCCESS;

}

StatusCode VBFTagger::SelectInputJets(const xAOD::JetContainer& jets){

  ATH_MSG_INFO(" --- SelectInputJets --- ");

  std::vector<std::unique_ptr<const xAOD::Jet*>> jets_service;

  for(const auto *jet : jets){
    jets_service.push_back(std::make_unique<const xAOD::Jet*>(jet));
  }

  ATH_CHECK(SelectInputJets(jets_service));

  return StatusCode::SUCCESS;

}

StatusCode VBFTagger::SelectInputJets(std::vector<std::unique_ptr<const xAOD::Jet*>>& jets){

  ATH_MSG_INFO(" --- SelectInputJets --- ");

  int count (0);

  // reset the sequences
  m_jets_pT = {}; m_jets_eta = {}; m_jets_phi = {}; m_jets_E = {};

  // find jets to remove from the sequence
  if(m_PreProcJetsMethod == "RemoveWJets")
    ATH_CHECK(CloseBosonMassJets(jets, m_mW));
  else if(m_PreProcJetsMethod == "RemoveZJets")
    ATH_CHECK(CloseBosonMassJets(jets, m_mZ));
  else if(m_PreProcJetsMethod == "RemovehJets")
    ATH_CHECK(CloseBosonMassJets(jets, m_mh));
  else if(m_PreProcJetsMethod == "RemoveFirst2Jets"){
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(*jets.at(0)) );
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(*jets.at(1)) );
  }
  else if(m_PreProcJetsMethod == "RemoveFirst4Jets"){
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(*jets.at(0)) );
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(*jets.at(1)) );
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(*jets.at(2)) );
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(*jets.at(3)) );
  }

  // first leading pT jets
  for(int i=0; i<jets.size(); i++){
    const xAOD::Jet* jet = *jets.at(i);

    // skip jets failing the kinematic range
    if(jet -> pt() < m_pTCut) continue;

    // skip jets to discard
    bool discard = false;
    for(long unsigned int ddd=0; ddd<m_JetsToDiscard.size(); ddd++){
      if(jet == *m_JetsToDiscard.at(ddd)) {
        if(m_IsDebug){
          ATH_MSG_INFO("   - skipping jet: " << jet -> pt()*0.001);
        }
        discard = true;
      }
    }
    if(discard) continue;

    // fill the rnn input jets
    m_jets_pT.push_back( jet -> pt() * 0.001 );
    m_jets_eta.push_back( jet -> eta() );
    m_jets_phi.push_back( jet -> phi() );
    m_jets_E.push_back( jet -> e() * 0.001 );

    if(m_IsDebug){
      ATH_MSG_INFO("   - filling jet: " << jet -> pt()*0.001);
    }

    count++;
    if(count == m_nMaxJets) break;
  }
  // make sure they are pT-sorted
  // ToDo...

  m_nRNNJets = m_jets_pT.size();

  ATH_MSG_INFO("   - nRNNJets: " << m_nRNNJets);

  return StatusCode::SUCCESS;

}

StatusCode VBFTagger::CloseBosonMassJets(std::vector<std::unique_ptr<const xAOD::Jet*>>& jets, int mBoson){

  if(m_IsDebug)
    ATH_MSG_INFO("VBFTagger::CloseBosonMassJets");

  // init
  float min_mass (999999.);
  const xAOD::Jet *jet1 = nullptr, *jet2 = nullptr; 
  m_JetsToDiscard = std::vector<std::unique_ptr<const xAOD::Jet*>>();

  // loop over jets
  for(int i=0; i<jets.size(); i++){
    const xAOD::Jet* ijet = *jets.at(i);

    // skip jets failing the kinematic range
    if(ijet -> pt() < m_pTCut) continue;

    for(int j=0; j<jets.size(); j++){
      const xAOD::Jet* jjet = *jets.at(j);
      if(jjet -> pt() < m_pTCut) continue;
      if(ijet == jjet) continue;

      float jj_m = (ijet -> p4() + jjet -> p4()).M()*0.001;
      if( abs(jj_m - mBoson) < min_mass ){
        min_mass = abs(jj_m - mBoson);
        jet1 = ijet;
        jet2 = jjet;
      }
    }  
  }

  // debug
  if(m_IsDebug){
    if( jet1 && jet2 ){
      ATH_MSG_INFO("   - jet1: " << jet1 -> pt()*0.001);
      ATH_MSG_INFO("   - jet2: " << jet2 -> pt()*0.001);
    }
  }

  // store the jets found
  if( jet1 && jet2 ){
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(jet1) );
    m_JetsToDiscard.push_back( std::make_unique<const xAOD::Jet*>(jet2) );
  }

  return StatusCode::SUCCESS;

}

int VBFTagger::GetNRNNJets(){
  return m_nRNNJets;
}

float VBFTagger::GetRNNScore(){
  return m_RNNScore;
}

StatusCode VBFTagger::finalize() { 
  return StatusCode::SUCCESS; 
}
