/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Antonio Giannini

#include "VBFTagger/VBFTaggerAlg.h"

#include <AthContainers/ConstDataVector.h>

VBFTaggerAlg::VBFTaggerAlg (const std::string& name, ISvcLocator* pSvcLocator)
    : AnaAlgorithm (name, pSvcLocator)
{
}

StatusCode VBFTaggerAlg::initialize ()
{
 
    // init event info and jets containers keys
    ATH_CHECK(m_EventInfoKey.initialize());

    ATH_CHECK(m_containerAllJetsKey.initialize());
    if( !m_containerSigJetsKey.empty() )
        ATH_CHECK(m_containerSigJetsKey.initialize());
    if( !m_containerSigLargeRJetsKey.empty() )
        ATH_CHECK(m_containerSigLargeRJetsKey.initialize());

    // retrieve the tool
    ANA_CHECK(m_vbftagger.retrieve());

    return StatusCode::SUCCESS;
}

StatusCode VBFTaggerAlg::execute ()
{

    // init containers
    auto workSigJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>();
    auto workSigLargeRJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>();

    // container we read in
    SG::ReadHandle<ConstDataVector<xAOD::JetContainer>> AllJetsContainer(m_containerAllJetsKey);
    ATH_CHECK(AllJetsContainer.isValid());

    // set work containers
    auto workAllJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
        AllJetsContainer->begin(), AllJetsContainer->end(), SG::VIEW_ELEMENTS);

    // do the same for signal small-R jets (if needed)
    if( !m_containerSigJetsKey.empty() ){
        SG::ReadHandle<ConstDataVector<xAOD::JetContainer>> SigJetsContainer(m_containerSigJetsKey);
        ATH_CHECK(SigJetsContainer.isValid());

        workSigJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
        SigJetsContainer->begin(), SigJetsContainer->end(), SG::VIEW_ELEMENTS);
    }

    // do the same for signal large-R jets (if needed)
    if( !m_containerSigLargeRJetsKey.empty() ){
        SG::ReadHandle<ConstDataVector<xAOD::JetContainer>> SigLargeRJetsContainer(m_containerSigLargeRJetsKey);
        ATH_CHECK(SigLargeRJetsContainer.isValid());

        // support this option in case analyses use only the leading pT large-R jet 
        // and they do not have a dedicate container for the signal large-R jet
        if(m_OnlyFirstLargeRJet){
            workSigLargeRJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
                SigLargeRJetsContainer->begin(), SigLargeRJetsContainer->begin()++, SG::VIEW_ELEMENTS);
        }
        else{
            workSigLargeRJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
                SigLargeRJetsContainer->begin(), SigLargeRJetsContainer->end(), SG::VIEW_ELEMENTS);
        }
    }

    // build the input sequences
    std::vector<double> pT, eta, phi, E;

    int count (0);
    for (const xAOD::Jet *jet : *workAllJetsContainer){

        // skip jets not passing the pT thr
        if( jet -> pt() < m_pTCut ) continue;

        // skip signal jets
        bool discard = false;
        for (const xAOD::Jet *jet_td : *workSigJetsContainer){
            if( jet == jet_td ) discard = true;
        }
        if(discard) continue;

        // skip small-R jets overlapping with signal large-R jets
        discard = false;
        for (const xAOD::Jet *jet_td : *workSigLargeRJetsContainer){
            if( jet -> p4().DeltaR(jet_td -> p4()) < m_DRCut ) discard = true;
        }
        if(discard) continue;

        // fill
        pT.push_back( jet -> pt() * 0.001 );
        eta.push_back( jet -> eta() );
        phi.push_back( jet -> phi() );
        E.push_back( jet -> e() * 0.001 );

        // check max sequence
        count ++;
        if( count == m_nMaxJets ) break;
    }

    // set the input jets
    ATH_CHECK(m_vbftagger -> SetInputSequences(pT, eta, phi, E));

    // do the job
    ATH_CHECK(m_vbftagger -> execute());

    // pick the event info container
    SG::ReadHandle<xAOD::EventInfo> eventInfo(m_EventInfoKey);
    ATH_CHECK(eventInfo.isValid());

    // decorate the score
    SG::AuxElement::Decorator<float> dec_vbfrnnscore("VBFRNNScore" + m_DecTag);
    dec_vbfrnnscore(*eventInfo) = m_vbftagger -> GetRNNScore();

    SG::AuxElement::Decorator<float> dec_nrnnjets("nRNNJets" + m_DecTag);
    //dec_nrnnjets(*eventInfo) = m_vbftagger -> GetNRNNJets();
    dec_nrnnjets(*eventInfo) = pT.size();

    return StatusCode::SUCCESS;
}
