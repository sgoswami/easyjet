/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Antonio Giannini

#include "VBFTagger/VBFTaggerAlgSys.h"

#include <AthContainers/ConstDataVector.h>

VBFTaggerAlgSys::VBFTaggerAlgSys (const std::string& name, ISvcLocator* pSvcLocator)
    : AnaAlgorithm (name, pSvcLocator)
{
}

StatusCode VBFTaggerAlgSys::initialize ()
{
 
    // Read syst-aware input handles
    ATH_CHECK (m_EventInfoKey.initialize(m_systematicsList));
    ATH_CHECK (m_containerAllJetsKey.initialize(m_systematicsList));
    if( !m_containerSigJetsKey.empty() )
        ATH_CHECK (m_containerSigJetsKey.initialize(m_systematicsList));
    if( !m_containerSigLargeRJetsKey.empty() )
        ATH_CHECK (m_containerSigLargeRJetsKey.initialize(m_systematicsList));

    m_RNNScoreDec = CP::SysWriteDecorHandle<float>{"RNNScore" + m_DecTag + "_%SYS%", this};
    ATH_CHECK(m_RNNScoreDec.initialize(m_systematicsList, m_EventInfoKey));

    m_nRNNJetsDec = CP::SysWriteDecorHandle<float>{"nRNNJets" + m_DecTag + "_%SYS%", this};
    ATH_CHECK(m_nRNNJetsDec.initialize(m_systematicsList, m_EventInfoKey));

    // Intialise syst list (must come after all syst-aware inputs and outputs)
    ANA_CHECK (m_systematicsList.initialize());

    // retrieve the tool
    ANA_CHECK(m_vbftagger.retrieve());

    return StatusCode::SUCCESS;
}

StatusCode VBFTaggerAlgSys::execute ()
{

    // Run over all systs
    for (const auto& sys : m_systematicsList.systematicsVector()){

        // Retrieve inputs
        const xAOD::JetContainer *AllJetsContainer = nullptr;
        ANA_CHECK (m_containerAllJetsKey.retrieve (AllJetsContainer, sys));

        // init containers
        auto workSigJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>();
        auto workSigLargeRJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>();

        const xAOD::JetContainer *SigJetsContainer = nullptr;
        const xAOD::JetContainer *SigLargeRJetsContainer = nullptr;

        // set work containers
        auto workAllJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
            AllJetsContainer->begin(), AllJetsContainer->end(), SG::VIEW_ELEMENTS);
    
        // do the same for signal small-R jets (if needed)
        if( !m_containerSigJetsKey.empty() ){
            ANA_CHECK (m_containerSigJetsKey.retrieve (SigJetsContainer, sys));
            workSigJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
            SigJetsContainer->begin(), SigJetsContainer->end(), SG::VIEW_ELEMENTS);
        }

        // do the same for signal large-R jets (if needed)
        if( !m_containerSigLargeRJetsKey.empty() ){
            ANA_CHECK (m_containerSigLargeRJetsKey.retrieve (SigLargeRJetsContainer, sys));
            // support this option in case analyses use only the leading pT large-R jet 
            // and they do not have a dedicate container for the signal large-R jet
            if(m_OnlyFirstLargeRJet){
                workSigLargeRJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
                    SigLargeRJetsContainer->begin(), SigLargeRJetsContainer->begin()++, SG::VIEW_ELEMENTS);
            }
            else{
                workSigLargeRJetsContainer = std::make_unique<ConstDataVector<xAOD::JetContainer>>(
                    SigLargeRJetsContainer->begin(), SigLargeRJetsContainer->end(), SG::VIEW_ELEMENTS);
            }
        }

        // build the input sequences
        std::vector<double> pT, eta, phi, E;

        int count (0);
        for (const xAOD::Jet *jet : *workAllJetsContainer){

            // skip jets not passing the pT thr
            if( jet -> pt() < m_pTCut ) continue;

            // skip signal jets
            bool discard = false;
            for (const xAOD::Jet *jet_td : *workSigJetsContainer){
                if( jet == jet_td ) discard = true;
            }
            if(discard) continue;

            // skip small-R jets overlapping with signal large-R jets
            discard = false;
            for (const xAOD::Jet *jet_td : *workSigLargeRJetsContainer){
                if( jet -> p4().DeltaR(jet_td -> p4()) < m_DRCut ) discard = true;
            }
            if(discard) continue;

            // fill
            pT.push_back( jet -> pt() * 0.001 );
            eta.push_back( jet -> eta() );
            phi.push_back( jet -> phi() );
            E.push_back( jet -> e() * 0.001 );

            // check max sequence
            count ++;
            if( count == m_nMaxJets ) break;
        }

        // set the input jets
        ATH_CHECK(m_vbftagger -> SetInputSequences(pT, eta, phi, E));

        // do the job
        ATH_CHECK(m_vbftagger -> execute());

        // pick the event info container
        const xAOD::EventInfo *eventInfo = nullptr;
        ANA_CHECK (m_EventInfoKey.retrieve (eventInfo, sys));

        // decorate the score
        m_RNNScoreDec.set(*eventInfo, m_vbftagger -> GetRNNScore(), sys);
        //m_nRNNJetsDec.set(*eventInfo, m_vbftagger -> GetNRNNJets(), sys);
        m_nRNNJetsDec.set(*eventInfo, pT.size(), sys);

    }

    return StatusCode::SUCCESS;
}
