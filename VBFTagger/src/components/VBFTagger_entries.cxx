///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/
// Author: Antonio Giannini
// Email : antonio.giannini@cern.ch
///////////////////////////////////////////////////////////////////
#include <VBFTagger/VBFTagger.h>
#include <VBFTagger/VBFTaggerAlg.h>
#include <VBFTagger/VBFTaggerAlgSys.h>

DECLARE_COMPONENT (VBFTagger)
DECLARE_COMPONENT (VBFTaggerAlg)
DECLARE_COMPONENT (VBFTaggerAlgSys)
