#setupATLAS -q
#lsetup git
#git init

git remote set-url origin https://:@gitlab.cern.ch:8443/sgoswami/easyjet.git

git branch -M main
#git rm -r --cached .
cd ../
git add .
date=$(date '+%Y-%m-%d %H:%M:%S')
git commit -m "Commit at $(date) "
git push --set-upstream origin main
