runConfig="MonobcAnalysis/RunConfig-Monobc.yaml"
executable="Monobc-ntupler"
campaignName="Monobc_v04"

# #data
# easyjet-gridsubmit --data-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/Data_Run2_p5855.txt \
#     --run-config ${runConfig} \
#     --exec ${executable} \
#     --nGBperJob 5 \
#     --campaign ${campaignName} \
#     --noTag

#Diboson
easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/Diboson_Run2_p5855.txt \
    --run-config ${runConfig} \
    --exec ${executable} \
    --nGBperJob 5 \
    --campaign ${campaignName} \
    --noTag

#Dijet
easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/Dijet_Run2_p5855.txt \
    --run-config ${runConfig} \
    --exec ${executable} \
    --nGBperJob 5 \
    --campaign ${campaignName} \
    --noTag

#Signal LQ
easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/LQ_Run2_p5855.txt \
    --run-config ${runConfig} \
    --exec ${executable} \
    --nGBperJob 5 \
    --campaign ${campaignName} \
    --noTag

# #single-top
# easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/SingleTop_Run2_p5855.txtt \
#     --run-config ${runConfig} \
#     --exec ${executable} \
#     --nGBperJob 5 \
#     --campaign ${campaignName} \
#     --noTag

#ttbar
easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/Ttbar_Run2_p5855.txt \
    --run-config ${runConfig} \
    --exec ${executable} \
    --nGBperJob 5 \
    --campaign ${campaignName} \
    --noTag

#W+jet
easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/WJets_Run2_p5855.txt \
    --run-config ${runConfig} \
    --exec ${executable} \
    --nGBperJob 5 \
    --campaign ${campaignName} \
    --noTag

#Z+jet (Sherpa)
easyjet-gridsubmit --mc-list ../easyjet/MonobcAnalysis/datasets/PHYS/MC20e/Zjets_Sh_Run2_p5855.txt \
    --run-config ${runConfig} \
    --exec ${executable} \
    --nGBperJob 5 \
    --campaign ${campaignName} \
    --noTag