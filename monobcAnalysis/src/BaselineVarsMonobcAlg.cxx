/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "BaselineVarsMonobcAlg.h"

#include "AthContainers/AuxElement.h"
#include <AthContainers/ConstDataVector.h>

#include "TLorentzVector.h"

namespace MONOBC
{
  BaselineVarsMonobcAlg::BaselineVarsMonobcAlg(const std::string& name,
    ISvcLocator* pSvcLocator)
    : AthHistogramAlgorithm(name, pSvcLocator)
  {

  }

  StatusCode BaselineVarsMonobcAlg::initialize()
  {
    // Read syst-aware input handles
    ATH_CHECK(m_jetHandle.initialize(m_systematicsList));
    ATH_CHECK(m_electronHandle.initialize(m_systematicsList));
    ATH_CHECK(m_muonHandle.initialize(m_systematicsList));
    ATH_CHECK(m_tauHandle.initialize(m_systematicsList));
    ATH_CHECK(m_metHandle.initialize(m_systematicsList));
    ATH_CHECK(m_eventHandle.initialize(m_systematicsList));

    if (m_isMC) {
      m_ele_SF = CP::SysReadDecorHandle<float>("el_effSF_" + m_eleWPName + "_%SYS%", this);
    }
    ATH_CHECK(m_ele_SF.initialize(m_systematicsList, m_electronHandle, SG::AllowEmpty));

    if (m_isMC) {
      m_mu_SF = CP::SysReadDecorHandle<float>("muon_effSF_" + m_muWPName + "_%SYS%", this);
    }
    ATH_CHECK(m_mu_SF.initialize(m_systematicsList, m_muonHandle, SG::AllowEmpty));

    if (m_isMC) {
      m_tau_SF = CP::SysReadDecorHandle<float>("tau_effSF_" + m_tauWPName + "_%SYS%", this);
    }
    ATH_CHECK(m_tau_SF.initialize(m_systematicsList, m_tauHandle, SG::AllowEmpty));

    if (!m_isBtag.empty()) {
      ATH_CHECK(m_isBtag.initialize(m_systematicsList, m_jetHandle));
    }

    ATH_CHECK(m_METSig.initialize(m_systematicsList, m_metHandle));

    if (m_isMC) {
      ATH_CHECK(m_truthFlav.initialize(m_systematicsList, m_jetHandle));
    }

    // Intialise syst-aware output decorators
    for (const std::string& var : m_floatVariables) {
      CP::SysWriteDecorHandle<float> whandle{ var + "_%SYS%", this };
      m_Fbranches.emplace(var, whandle);
      ATH_CHECK(m_Fbranches.at(var).initialize(m_systematicsList, m_eventHandle));
    }

    for (const std::string& var : m_intVariables) {
      ATH_MSG_DEBUG("initializing integer variable: " << var);
      CP::SysWriteDecorHandle<int> whandle{ var + "_%SYS%", this };
      m_Ibranches.emplace(var, whandle);
      ATH_CHECK(m_Ibranches.at(var).initialize(m_systematicsList, m_eventHandle));
    };

    // Intialise syst list (must come after all syst-aware inputs and outputs)
    ATH_CHECK(m_systematicsList.initialize());

    return StatusCode::SUCCESS;
  }

  StatusCode BaselineVarsMonobcAlg::execute()
  {

    // Loop over all systs
    for (const auto& sys : m_systematicsList.systematicsVector())
    {

      // Retrieve inputs
      const xAOD::EventInfo* event = nullptr;
      ANA_CHECK(m_eventHandle.retrieve(event, sys));

      const xAOD::JetContainer* jets = nullptr;
      ANA_CHECK(m_jetHandle.retrieve(jets, sys));

      const xAOD::MuonContainer* muons = nullptr;
      ANA_CHECK(m_muonHandle.retrieve(muons, sys));

      const xAOD::ElectronContainer* electrons = nullptr;
      ANA_CHECK(m_electronHandle.retrieve(electrons, sys));

      const xAOD::TauJetContainer* taus = nullptr;
      ANA_CHECK(m_tauHandle.retrieve(taus, sys));

      const xAOD::MissingETContainer* metCont = nullptr;
      ANA_CHECK(m_metHandle.retrieve(metCont, sys));
      const xAOD::MissingET* met = (*metCont)["Final"];
      if (!met) {
        ATH_MSG_ERROR("Could not retrieve MET");
        return StatusCode::FAILURE;
      }

      for (const std::string& string_var : m_floatVariables) {
        m_Fbranches.at(string_var).set(*event, -99., sys);
      }

      for (const auto& var : m_intVariables) {
        m_Ibranches.at(var).set(*event, -99, sys);
      }

      static const SG::AuxElement::ConstAccessor<int>  HadronConeExclTruthLabelID("HadronConeExclTruthLabelID");

      TLorentzVector Leading_jet;
      TLorentzVector Subleading_jet;
      TLorentzVector met_vector;

      // Count leptons
      int n_electrons = electrons->size();
      int n_muons = muons->size();
      int n_taus = taus->size();

      // Count jets
      int n_jets = jets->size();

      // b-jet sector
      bool WPgiven = !m_isBtag.empty();
      auto bjets = std::make_unique<ConstDataVector<xAOD::JetContainer>>(SG::VIEW_ELEMENTS);
      for (const xAOD::Jet* jet : *jets) {
        if (WPgiven) {
          if (m_isBtag.get(*jet, sys) && std::abs(jet->eta()) < 2.5) bjets->push_back(jet);
        }
      }
      int n_bjets = bjets->size();

      m_Ibranches.at("nElectrons").set(*event, n_electrons, sys);
      m_Ibranches.at("nMuons").set(*event, n_muons, sys);
      m_Ibranches.at("nTaus").set(*event, n_taus, sys);
      m_Ibranches.at("nJets").set(*event, n_jets, sys);
      m_Ibranches.at("nBJets").set(*event, n_bjets, sys);


      //MET Significance
      float METSig = m_METSig.get(*met, sys);
      m_Fbranches.at("METSig").set(*event, METSig, sys);


      //jet sector
      TLorentzVector jj;
      for (int i = 0; i < std::min(n_jets, 2); i++) {
        m_Fbranches.at("Jet" + std::to_string(i + 1) + "_pt").set(*event, jets->at(i)->pt(), sys);
        m_Fbranches.at("Jet" + std::to_string(i + 1) + "_eta").set(*event, jets->at(i)->eta(), sys);
        m_Fbranches.at("Jet" + std::to_string(i + 1) + "_phi").set(*event, jets->at(i)->phi(), sys);
        m_Fbranches.at("Jet" + std::to_string(i + 1) + "_E").set(*event, jets->at(i)->e(), sys);
      }
      if (n_jets >= 2) {
        jj = jets->at(0)->p4() + jets->at(1)->p4();
        m_Fbranches.at("mjj").set(*event, jj.M(), sys);
        m_Fbranches.at("pTjj").set(*event, jj.Pt(), sys);
        m_Fbranches.at("Etajj").set(*event, jj.Eta(), sys);
        m_Fbranches.at("Phijj").set(*event, jj.Phi(), sys);
        m_Fbranches.at("dRjj").set(*event, (jets->at(0)->p4()).DeltaR(jets->at(1)->p4()), sys);
        m_Fbranches.at("dEtajj").set(*event, (jets->at(0)->eta()) - (jets->at(1)->eta()), sys);
        m_Fbranches.at("dPhijj").set(*event, (jets->at(0)->p4()).DeltaPhi(jets->at(1)->p4()), sys);
      }

      //b-jet sector
      for (int i = 0; i < std::min(n_bjets, 2); i++) {
        m_Fbranches.at("Jet_b" + std::to_string(i + 1) + "_pt").set(*event, bjets->at(i)->pt(), sys);
        m_Fbranches.at("Jet_b" + std::to_string(i + 1) + "_eta").set(*event, bjets->at(i)->eta(), sys);
        m_Fbranches.at("Jet_b" + std::to_string(i + 1) + "_phi").set(*event, bjets->at(i)->phi(), sys);
        m_Fbranches.at("Jet_b" + std::to_string(i + 1) + "_E").set(*event, bjets->at(i)->e(), sys);
        if (m_isMC) {
          m_Ibranches.at("Jet_b" + std::to_string(i + 1) + "_truthLabel").set(*event, m_truthFlav.get(*bjets->at(i), sys), sys);
        }
      }
      if (n_bjets >= 2) {
        TLorentzVector bb = bjets->at(0)->p4() + bjets->at(1)->p4();
        m_Fbranches.at("mbb").set(*event, bb.M(), sys);
        m_Fbranches.at("pTbb").set(*event, bb.Pt(), sys);
        m_Fbranches.at("Etabb").set(*event, bb.Eta(), sys);
        m_Fbranches.at("Phibb").set(*event, bb.Phi(), sys);
        m_Fbranches.at("dRbb").set(*event, (bjets->at(0)->p4()).DeltaR(bjets->at(1)->p4()), sys);
        m_Fbranches.at("dPhibb").set(*event, (bjets->at(0)->p4()).DeltaPhi(bjets->at(1)->p4()), sys);
        m_Fbranches.at("dEtabb").set(*event, (bjets->at(0)->eta()) - bjets->at(1)->eta(), sys);
      }

    }
    return StatusCode::SUCCESS;
  }

  template<typename ParticleType>
  std::pair<int, int> BaselineVarsMonobcAlg::truthOrigin(const ParticleType* particle) {
    static const SG::AuxElement::ConstAccessor<int> lepttruthOrigin("truthOrigin");
    static const SG::AuxElement::ConstAccessor<int> lepttruthType("truthType");

    return { lepttruthOrigin(*particle), lepttruthType(*particle) };
  }

}
