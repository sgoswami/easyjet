/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

// Always protect against multiple includes!

#ifndef MONOBCANALYSIS_MONOBCSELECTORALG
#define MONOBCANALYSIS_MONOBCSELECTORALG

#include <memory>
#include "AnaAlgorithm/AnaAlgorithm.h"
#include <AsgDataHandles/ReadDecorHandleKey.h>

#include <SystematicsHandles/SysReadHandle.h>
#include <SystematicsHandles/SysListHandle.h>
#include <SystematicsHandles/SysWriteDecorHandle.h>
#include <SystematicsHandles/SysReadDecorHandle.h>
#include <SystematicsHandles/SysFilterReporterParams.h>

#include <xAODEventInfo/EventInfo.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODTau/TauJetContainer.h>

#include "TriggerMatchingTool/IMatchingTool.h"
#include <EasyjetHub/CutManager.h>

namespace MONOBC
{
  enum TriggerChannel
  {
    MET_trig,
  };

  enum Booleans
  {
    PASS_TRIGGER,
    JETCUT,
    METCUT,
    LEPTON_VETO   
  };

  /// \brief An algorithm for counting containers
  class MonobcSelectorAlg final : public EL::AnaAlgorithm {

    public:
      MonobcSelectorAlg(const std::string &name, ISvcLocator *pSvcLocator);

      /// \brief Initialisation method, for setting up tools and other persistent
      /// configs
      StatusCode initialize() override;
      /// \brief Execute method, for actions to be taken in the event loop
      StatusCode execute() override;
      /// \brief This is the mirror of initialize() and is called after all events are processed.
      StatusCode finalize() override; ///I added this to write the cutflow histogram.

      const std::vector<std::string> m_STANDARD_CUTS{
        "PASS_TRIGGER",
        "JETCUT",  
        "METCUT",
        "LEPTON_VETO",      
      };


    private :
      // ToolHandle<whatever> handle {this, "pythonName", "defaultValue",
      // "someInfo"};

      Gaudi::Property<bool> m_isMC
      { this, "isMC", false, "Is this simulation?" };

      Gaudi::Property<bool> m_bypass
      { this, "bypass", false, "Run selector algorithm in pass-through mode" };

      /// \brief Setup syst-aware input container handles
      CP::SysListHandle m_systematicsList {this};

      CP::SysReadHandle<xAOD::JetContainer>
      m_jetHandle{ this, "jets", "",   "Jet container to read" };

      CP::SysReadDecorHandle<char>
      m_isBtag {this, "bTagWPDecorName", "", "Name of input dectorator for b-tagging"};

      CP::SysReadHandle<xAOD::EventInfo>
      m_eventHandle{ this, "event", "EventInfo",   "EventInfo container to read" };

      CP::SysReadHandle<xAOD::ElectronContainer>
      m_electronHandle{ this, "electrons", "",   "Electron container to read" };

      CP::SysReadHandle<xAOD::MuonContainer>
      m_muonHandle{ this, "muons", "",   "Muon container to read" };

      CP::SysReadHandle<xAOD::TauJetContainer>
      m_tauHandle{ this, "taus", "",   "Tau container to read" };

      CP::SysReadHandle<xAOD::MissingETContainer>
      m_metHandle{ this, "met", "AnalysisMET",   "MET container to read" };

      CP::SysReadDecorHandle<unsigned int> m_year {this, "year", "dataTakingYear", ""};

      CP::SysFilterReporterParams m_filterParams {this, "Monobc selection"};

      std::unordered_map<std::string,  SG::ReadDecorHandleKey<xAOD::EventInfo>> m_triggerDecorKeys;

      std::unordered_map<MONOBC::TriggerChannel, std::string> m_triggerChannels =
      {
        {MONOBC::MET_trig, "MET_trig"},
      };

      Gaudi::Property<std::vector<std::string>> m_triggers
      { this, "triggerLists", {}, "Name list of trigger" };

      std::unordered_map<std::string, CP::SysReadDecorHandle<bool> > m_triggerdecos;

      long long int m_total_events{0};

      std::unordered_map<MONOBC::Booleans, CP::SysWriteDecorHandle<bool> > m_Bbranches;
      std::unordered_map<MONOBC::Booleans, bool> m_bools;
      std::unordered_map<MONOBC::Booleans, std::string> m_boolnames{
        {MONOBC::PASS_TRIGGER, "PASS_TRIGGER"},
        {MONOBC::JETCUT, "JETCUT"},
        {MONOBC::METCUT, "METCUT"},
        {MONOBC::LEPTON_VETO, "LEPTON_VETO"},
      };

      CutManager m_MonobcCuts;
      Gaudi::Property<std::vector<std::string>> m_inputCutList{this, "cutList", {}};
      std::vector<MONOBC::Booleans> m_inputCutKeys;
      Gaudi::Property<bool> m_saveCutFlow{this, "saveCutFlow", false};
      CP::SysWriteDecorHandle<bool> m_passallcuts {"PassAllCuts_%SYS%", this};

      void evaluateTriggerCuts
	      (const xAOD::EventInfo* event, const xAOD::MissingET* met,
	       CutManager& MonobcCuts, const CP::SystematicSet& sys);
      void evaluateLeptonVeto
        (const xAOD::ElectronContainer& electrons, const xAOD::MuonContainer& muons,
         const xAOD::TauJetContainer& taus, CutManager& MonobcCuts);
      void evaluateMETCuts
        (const xAOD::MissingET* met, CutManager& MonobcCuts);
      void evaluateJETCuts
        (const xAOD::JetContainer& jets, CutManager& MonobcCuts);
  };
}

#endif // MONOBCANALYSIS_MONOBCSELECTORALG

