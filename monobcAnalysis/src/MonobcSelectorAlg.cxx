/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "MonobcSelectorAlg.h"
#include <AthenaKernel/Units.h>

#include <AsgDataHandles/ReadDecorHandle.h>
#include <SystematicsHandles/SysFilterReporter.h>
#include <SystematicsHandles/SysFilterReporterCombiner.h>
#include <AthContainers/ConstDataVector.h>


namespace MONOBC
{

  MonobcSelectorAlg::MonobcSelectorAlg(const std::string& name,
    ISvcLocator* pSvcLocator)
    : EL::AnaAlgorithm(name, pSvcLocator)
  {
  }


  StatusCode MonobcSelectorAlg::initialize()
  {
    ATH_MSG_INFO("*********************************\n");
    ATH_MSG_INFO("     MonobcSelectorAlg      \n");
    ATH_MSG_INFO("*********************************\n");

    // Initialise global event filter
    ATH_CHECK(m_filterParams.initialize(m_systematicsList));

    ATH_CHECK(m_jetHandle.initialize(m_systematicsList));
    if (!m_isBtag.empty()) {
      ATH_CHECK(m_isBtag.initialize(m_systematicsList, m_jetHandle));
    }

    ATH_CHECK(m_electronHandle.initialize(m_systematicsList));
    ATH_CHECK(m_muonHandle.initialize(m_systematicsList));
    ATH_CHECK(m_tauHandle.initialize(m_systematicsList));
    ATH_CHECK(m_metHandle.initialize(m_systematicsList));
    ATH_CHECK(m_eventHandle.initialize(m_systematicsList));
    ATH_CHECK(m_year.initialize(m_systematicsList, m_eventHandle));

    for (auto& [key, value] : m_boolnames) {
      m_bools.emplace(key, false);
      CP::SysWriteDecorHandle<bool> whandle{ value + "_%SYS%", this };
      m_Bbranches.emplace(key, whandle);
      ATH_CHECK(m_Bbranches.at(key).initialize(m_systematicsList, m_eventHandle));
    }

    // make trigger decorators
    for (auto trig : m_triggers) {
      CP::SysReadDecorHandle<bool> deco{ this, "trig" + trig, trig, "Name of trigger" };
      m_triggerdecos.emplace(trig, deco);
      ATH_CHECK(m_triggerdecos.at(trig).initialize(m_systematicsList, m_eventHandle));
    }

    // special flag for all cuts
    ATH_CHECK(m_passallcuts.initialize(m_systematicsList, m_eventHandle));

    // Intialise syst list (must come after all syst-aware inputs and outputs)
    ATH_CHECK(m_systematicsList.initialize());

    std::vector<std::string> boolnameslist;
    for (const auto& [key, value] : m_boolnames) {
      boolnameslist.push_back(value);
    }
    m_MonobcCuts.CheckInputCutList(m_inputCutList, boolnameslist);

    m_inputCutKeys.resize(m_inputCutList.size());
    std::vector<bool> inputWasFound(m_inputCutList.size(), false);
    for (const auto& [key, value] : m_boolnames) {
      auto it = std::find(m_inputCutList.begin(), m_inputCutList.end(), value);
      if (it != m_inputCutList.end()) {
        auto index = it - m_inputCutList.begin();
        m_inputCutKeys.at(index) = key;
        inputWasFound.at(index) = true;
      }
    }

    for (unsigned int index = 0; index < inputWasFound.size(); index++) {
      if (inputWasFound.at(index)) continue;
      ATH_MSG_ERROR("Doubled or falsely spelled cuts in CutList (see config file)." + m_inputCutList[index]);
    }

    for (const auto& cut : m_inputCutKeys) {
      m_MonobcCuts.add(m_boolnames[cut]);
    }

    //After filling the CutManager, book your histograms.
    const unsigned int nbins = m_MonobcCuts.size() + 1; //  need an extra bin for the total num of events.
    ANA_CHECK(book(TEfficiency("AbsoluteEfficiency", "Absolute Efficiency of Monobc cuts;Cuts;#epsilon",
      nbins, 0.5, nbins + 0.5)));
    ANA_CHECK(book(TEfficiency("RelativeEfficiency", "Relative Efficiency of Monobc cuts;Cuts;#epsilon",
      nbins, 0.5, nbins + 0.5)));
    ANA_CHECK(book(TEfficiency("StandardCutFlow", "StandardCutFlow of Monobc cuts;Cuts;#epsilon",
      nbins, 0.5, nbins + 0.5)));
    ANA_CHECK(book(TH1F("EventsPassed_BinLabeling", "Events passed by each cut / Bin labeling", nbins, 0.5, nbins + 0.5)));

    return StatusCode::SUCCESS;
  }


  StatusCode MonobcSelectorAlg::execute()
  {
    // Global filter originally false
    CP::SysFilterReporterCombiner filterCombiner(m_filterParams, false);

    // Loop over all systs
    for (const auto& sys : m_systematicsList.systematicsVector())
    {
      CP::SysFilterReporter filter(filterCombiner, sys);

      // Retrive inputs
      const xAOD::EventInfo* event = nullptr;
      ANA_CHECK(m_eventHandle.retrieve(event, sys));

      const xAOD::JetContainer* jets = nullptr;
      ANA_CHECK(m_jetHandle.retrieve(jets, sys));

      bool WPgiven = !m_isBtag.empty();
      auto bjets = std::make_unique<ConstDataVector<xAOD::JetContainer>>(SG::VIEW_ELEMENTS);
      for (const xAOD::Jet* jet : *jets) {
        if (WPgiven) {
          if (m_isBtag.get(*jet, sys) && std::abs(jet->eta()) < 2.5) bjets->push_back(jet);
        }
      }

      const xAOD::MuonContainer *muons = nullptr;
      ANA_CHECK(m_muonHandle.retrieve(muons, sys));

      const xAOD::ElectronContainer *electrons = nullptr;
      ANA_CHECK(m_electronHandle.retrieve(electrons, sys));

      const xAOD::TauJetContainer *taus = nullptr;
      ANA_CHECK(m_tauHandle.retrieve(taus, sys));

      const xAOD::MissingETContainer *metCont = nullptr;
      ANA_CHECK(m_metHandle.retrieve(metCont, sys));
      const xAOD::MissingET* met = (*metCont)["Final"]; // To check
      if (!met) {
        ATH_MSG_ERROR("Could not retrieve MET");
        return StatusCode::FAILURE;
      }

      m_bools.at(MONOBC::PASS_TRIGGER) = false;
      m_bools.at(MONOBC::JETCUT) = false;
      m_bools.at(MONOBC::METCUT) = false;
      m_bools.at(MONOBC::LEPTON_VETO) = false;

      evaluateTriggerCuts(event, met, m_MonobcCuts, sys);
      evaluateJETCuts(*jets, m_MonobcCuts);
      evaluateMETCuts(met, m_MonobcCuts);
      evaluateLeptonVeto(*electrons, *muons, *taus, m_MonobcCuts);


      bool passedall = true;
      for (const auto& [key, value] : m_boolnames) {
        auto it = std::find(m_STANDARD_CUTS.begin(), m_STANDARD_CUTS.end(), value);
        if (it != m_STANDARD_CUTS.end()) {
          passedall &= m_bools.at(key);
        }
      }
      m_passallcuts.set(*event, passedall, sys);

      bool pass_baseline = false;
      if (m_bools.at(MONOBC::PASS_TRIGGER) && m_bools.at(MONOBC::JETCUT) && m_bools.at(MONOBC::METCUT) && m_bools.at(MONOBC::LEPTON_VETO)) pass_baseline = true;

      // do the CUTFLOW only with sys="" -> NOSYS
      if (sys.name() == "") {

        // Compute total_events
        m_total_events += 1;

        for (const auto& cut : m_inputCutKeys) {
          if (m_MonobcCuts.exists(m_boolnames.at(cut))) {
            m_MonobcCuts(m_boolnames.at(cut)).passed = m_bools.at(cut);
            if (m_MonobcCuts(m_boolnames.at(cut)).passed) {
              m_MonobcCuts(m_boolnames.at(cut)).counter += 1;
            }
          }
        }
      }

      // Check how many consecutive cuts are passed by the event.
      unsigned int consecutive_cuts = 0;
      for (size_t i = 0; i < m_MonobcCuts.size(); ++i) {
        if (m_MonobcCuts[i].passed)
          consecutive_cuts++;
        else
          break;
      }

      // Here we basically increment the  N_events(pass_i  AND pass_i-1  AND ... AND pass_0) for the i-cut.
      // I think this is an elegant way to do it :) . Considering the difficulties a configurable cut list imposes.
      for (unsigned int i = 0; i < consecutive_cuts; i++) {
        m_MonobcCuts[i].relativeCounter += 1;
      }

      for (auto& [key, var] : m_bools) {
        m_Bbranches.at(key).set(*event, var, sys);
      }

      if (!m_bypass && !pass_baseline) continue;
      filter.setPassed(true);
    }
    return StatusCode::SUCCESS;
  }

  StatusCode MonobcSelectorAlg::finalize()
  {
    //adapt the following for each syst TODO
    ATH_MSG_INFO("Total events = " << m_total_events << std::endl);
    ANA_CHECK(m_filterParams.finalize());
    m_MonobcCuts.CheckCutResults(); // Print CheckCutResults

    if (m_saveCutFlow) {
      m_MonobcCuts.DoAbsoluteEfficiency(m_total_events, efficiency("AbsoluteEfficiency"));
      m_MonobcCuts.DoRelativeEfficiency(m_total_events, efficiency("RelativeEfficiency"));
      m_MonobcCuts.DoStandardCutFlow(m_total_events, efficiency("StandardCutFlow"));
      m_MonobcCuts.DoCutflowLabeling(m_total_events, hist("EventsPassed_BinLabeling"));

    }
    else {
      delete efficiency("AbsoluteEfficiency");
      delete efficiency("RelativeEfficiency");
      delete efficiency("StandardCutFlow");
      delete hist("EventsPassed_BinLabeling");
    }

    return StatusCode::SUCCESS;
  }

  void MonobcSelectorAlg::evaluateTriggerCuts
  (const xAOD::EventInfo* event, const xAOD::MissingET* met, CutManager& MonobcCuts, const CP::SystematicSet& sys) {

    if (!MonobcCuts.exists("PASS_TRIGGER"))
      return;

    std::vector<std::string> met_paths;
    int year = m_year.get(*event, sys);

    if (2015 <= year && year <= 2018) {
      met_paths = { "HLT_xe110_pufit_xe70_L1XE50", "HLT_xe120_pufit_L1XE50" };
    }
    if (met) {
      for (const auto& trig : met_paths) {
        bool pass = m_triggerdecos.at("trigPassed_" + trig).get(*event, sys);
        if (pass) {
          m_bools.at(MONOBC::PASS_TRIGGER) = true;
        }
      }
      m_bools.at(MONOBC::PASS_TRIGGER) &= (met->met() > (200 * Athena::Units::GeV));
    }
  }

  void MonobcSelectorAlg::evaluateJETCuts
  (const xAOD::JetContainer& jets, CutManager& MonobcCuts)
  {
    if (MonobcCuts.exists("JETCUT") && (jets.size() >= 2) && (jets.at(0)->pt() > 150 * Athena::Units::GeV) && (jets.at(1)->pt() > 30 * Athena::Units::GeV)) {
      m_bools.at(MONOBC::JETCUT) = true;
    }
  }

  void MonobcSelectorAlg::evaluateMETCuts
  (const xAOD::MissingET* met, CutManager& MonobcCuts)
  {
    if (MonobcCuts.exists("METCUT") && (met->met() > 200 * Athena::Units::GeV)) {
      m_bools.at(MONOBC::METCUT) = true;
    }
  }

  void MonobcSelectorAlg::evaluateLeptonVeto(const xAOD::ElectronContainer& electrons,
    const xAOD::MuonContainer& muons,
    const xAOD::TauJetContainer& taus,
    CutManager& MonobcCuts)
  {
    if (!MonobcCuts.exists("LEPTON_VETO")) {
      return;
    }
    int n_leptons = electrons.size() + muons.size() + taus.size();
    if (n_leptons == 0) m_bools.at(MONOBC::LEPTON_VETO) = true;
  }
}