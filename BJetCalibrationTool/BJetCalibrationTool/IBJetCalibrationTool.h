///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

// IBJetCalibrationTool.h
// Interface file for class BJetCalibrationTool
// Author: TJ Khoo
///////////////////////////////////////////////////////////////////

#ifndef BJetCalibrationTool_IBJetCalibrationTool_H
#define BJetCalibrationTool_IBJetCalibrationTool_H 1

#include "AsgTools/IAsgTool.h"

// BJetCalibrationTool includes
#include <vector>
#include "xAODJet/JetContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODMuon/MuonContainer.h"

class IBJetCalibrationTool : public virtual asg::IAsgTool {
  ASG_TOOL_INTERFACE(CP::IBJetCalibrationTool)

public:

  virtual std::vector<const xAOD::Muon*> selectMuonsForCorrection(const xAOD::MuonContainer& muonsIn) = 0;
  virtual StatusCode applyBJetCalibration(xAOD::Jet& jet, const std::vector<const xAOD::Muon*>& muons_for_correction) = 0;
}; 

#endif //> !BJetCalibrationTool_APPLYBJETCALIBRATION_H
