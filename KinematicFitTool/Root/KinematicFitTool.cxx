///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitTool.cxx
// Source file for class KinematicFitTool
// Author: BELFKIR Mohamed
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

// KinematicFitTool includes
// #include <AsgTools/MessageCheck.h>

#include "KinematicFitTool/KinematicFitTool.h"
#include "PathResolver/PathResolver.h"

#include "TLorentzVector.h"
#include "xAODEgamma/Photon.h"
#include "xAODJet/Jet.h"

KinematicFitTool::KinematicFitTool(const std::string &name)
    : asg::AsgTool(name), m_JetAlgo(""), m_BtaggingWP(), m_Jet_Min_Pt(20.),
      m_angles_Res(0.01), m_isFixAngles(true),
      m_file_name("")
{
  declareProperty("JetCollection", m_JetAlgo = "AntiKt4EMPFlow");
  declareProperty("JetMinPt", m_Jet_Min_Pt = 20.);
  declareProperty("bTagWPDecorName",
                  m_BtaggingWP = "ftag_select_GN2v00LegacyWP_FixedCutBEff_77");
  declareProperty("AnglesResolution", m_angles_Res = .01);
  declareProperty("FixAnglesFit", m_isFixAngles = true);
  declareProperty("FilesName", m_file_name = "_TaOgataParameters.root");
}

KinematicFitTool::~KinematicFitTool() {}

StatusCode KinematicFitTool::initialize()
{
  ANA_MSG_INFO("Initializing KinematicFit ... " << name() << " ... ");
  ANA_CHECK(this->initializeTool(name()));
  return StatusCode::SUCCESS;
}

StatusCode KinematicFitTool::finalize() { return StatusCode::SUCCESS; }

StatusCode KinematicFitTool::initializeTool(const std::string &name)
{
  if (m_JetAlgo == "")
  {
    ANA_MSG_FATAL("KinematicFitTool::initialize() : Please set JetCollection");
    return StatusCode::FAILURE;
  }

  const std::string m_EnResp = PathResolverFindCalibFile(
      "KinematicFitTool/E_" + m_JetAlgo + m_file_name);
  const std::string m_PtResp = PathResolverFindCalibFile(
      "KinematicFitTool/pT_" + m_JetAlgo + m_file_name);
  const std::string m_pXconstr = PathResolverFindCalibFile(
      "KinematicFitTool/pXconstr_" + m_JetAlgo + "_TaOTaParameters.root");
  const std::string m_pYconstr = PathResolverFindCalibFile(
      "KinematicFitTool/pYconstr_" + m_JetAlgo + "_TaOTaParameters.root");

  fResolution = new KinematicFitResolution();
  ANA_CHECK(fResolution->setProperty("AnglesResolution", m_angles_Res));
  ANA_CHECK(fResolution->setProperty("FixAnglesFit", m_isFixAngles));
  ANA_CHECK(fResolution->setProperty("EnergyResponseFile", m_EnResp));
  ANA_CHECK(fResolution->setProperty("PtResponseFile", m_PtResp));
  ANA_CHECK(fResolution->setProperty("pXconstraintFile", m_pXconstr));
  ANA_CHECK(fResolution->setProperty("pYconstraintFile", m_pYconstr));
  ANA_CHECK(fResolution->initialize());

  fEvent = new KinematicFitEvent();
  ANA_CHECK(fEvent->setProperty("JetMinPt", m_Jet_Min_Pt));
  ANA_CHECK(fEvent->setProperty("bTagWPDecorName", m_BtaggingWP));
  ANA_CHECK(fEvent->initialize());

  ANA_MSG_INFO("KinematicFitTool::initialize() : " << name
                                                   << " is initialized!");

  ANA_MSG_INFO("KinematicFitTool::initialize() : KinematicFitRun is Ready!");
  return StatusCode::SUCCESS;
}

StatusCode KinematicFitTool::applyKF(const xAOD::PhotonContainer &photons,
                                     const xAOD::JetContainer &jets,
                                     Double_t &KF1_Mbb)
{
  static SG::AuxElement::Decorator<Float_t> PT("KF_PT");
  static SG::AuxElement::Decorator<Float_t> ETA("KF_ETA");
  static SG::AuxElement::Decorator<Float_t> PHI("KF_PHI");
  static SG::AuxElement::Decorator<Float_t> M("KF_M");
  static SG::AuxElement::Decorator<Char_t> isB("KF_isB");

  for (auto jet : jets)
  {
    PT(*jet) = jet->pt();
    ETA(*jet) = jet->eta();
    PHI(*jet) = jet->phi();
    M(*jet) = jet->m();
    isB(*jet) = false;
  }

  // --- For the Kinematic Fit Second run per event --- //
  std::vector<TLorentzVector> jetsCopy(jets.size());
  for (unsigned int i = 0; i < jetsCopy.size(); i++)
  {
    jetsCopy[i].SetPtEtaPhiE(jets[i]->pt(), jets[i]->eta(), jets[i]->phi(),
                             jets[i]->e());
  }

  std::vector<TLorentzVector> photCopy(photons.size());
  for (unsigned int i = 0; i < photCopy.size(); i++)
  {
    photCopy[i].SetPtEtaPhiE(photons[i]->pt(), photons[i]->eta(),
                             photons[i]->phi(), photons[i]->e());
  }

  for (int i = 1; i <= 2; i++)
  {
    if (i == 2)
    {
      for (unsigned int j = 0; j < jetsCopy.size(); j++)
      {

        PT(*jets[j]) = jetsCopy[j].Pt();
        ETA(*jets[j]) = jetsCopy[j].Eta();
        PHI(*jets[j]) = jetsCopy[j].Phi();
        M(*jets[j]) = jetsCopy[j].M();
        isB(*jets[j]) = false;
      }

    }
    if (!fEvent->applySelection(photons, jets))
      return StatusCode::SUCCESS;
    fProcessor = new KinematicFitRun();
    ANA_CHECK(fProcessor->setProperty("IncludingConstr", i));
    fProcessor->RunKF(fEvent, fResolution);
    DecorateEvent(fEvent, fProcessor, KF1_Mbb, i);
    if (i==2) {
      PT(*jets[0]) = (fEvent->GetBJetOne())->pt();
      ETA(*jets[0]) = (fEvent->GetBJetOne())->eta();
      PHI(*jets[0]) = (fEvent->GetBJetOne())->phi();
      M(*jets[0]) = (fEvent->GetBJetOne())->m();      
      PT(*jets[1]) = (fEvent->GetBJetTwo())->pt();
      ETA(*jets[1]) = (fEvent->GetBJetTwo())->eta();
      PHI(*jets[1]) = (fEvent->GetBJetTwo())->phi();
      M(*jets[1]) = (fEvent->GetBJetTwo())->m();      
      if (fEvent->GetAddJets().size() >= FitType::ThreeJet)
      {
        for (unsigned int j = 0; j < fEvent->GetAddJets().size(); j++)
        {
          PT(*jets[j + 2]) = (fEvent->GetAddJets().at(j))->pt();
          ETA(*jets[j + 2]) = (fEvent->GetAddJets().at(j))->eta();
          PHI(*jets[j + 2]) = (fEvent->GetAddJets().at(j))->phi();
	  M(*jets[j + 2]) = (fEvent->GetAddJets().at(j))->m();
        }
      }
    }

    fEvent->Clear();
    delete fProcessor;
  }
  return StatusCode::SUCCESS;
}

void KinematicFitTool::DecorateEvent(KinematicFitEvent *&Event,
                                     KinematicFitRun *&Run, Double_t &KF1_Mbb,
                                     int i)
{
  bool isFitted = Run->isFitted();
  if (isFitted)
  {
    TLorentzVector jet1;
    TLorentzVector jet2;

    jet1.SetPtEtaPhiE(
        (Event->GetFitBJetOne()).Pt(), (Event->GetFitBJetOne()).Eta(),
        (Event->GetFitBJetOne()).Phi(), (Event->GetFitBJetOne()).E());
    jet2.SetPtEtaPhiE(
        (Event->GetFitBJetTwo()).Pt(), (Event->GetFitBJetTwo()).Eta(),
        (Event->GetFitBJetTwo()).Phi(), (Event->GetFitBJetTwo()).E());

    TLorentzVector jj = jet1 + jet2;

    if (i == 1)
    {
      KF1_Mbb = jj.M();
    }

    else if (i == 2)
    {
      xAOD::JetFourMom_t jet14vec(jet1.Pt(), jet1.Eta(), jet1.Phi(), jet1.M());
      xAOD::JetFourMom_t jet24vec(jet2.Pt(), jet2.Eta(), jet2.Phi(), jet2.M());

      (Event->GetBJetOne())->setJetP4(jet14vec);
      (Event->GetBJetTwo())->setJetP4(jet24vec);

      if (Event->GetAddJets().size() >= FitType::ThreeJet)
      {
        std::vector<TLorentzVector> jets(Event->GetAddJets().size());
        for (unsigned int i = 0; i < Event->GetAddJets().size(); i++)
        {
          jets[i].SetPtEtaPhiE((Event->GetFitAddJet())[i].Pt(),
                               (Event->GetFitAddJet())[i].Eta(),
                               (Event->GetFitAddJet())[i].Phi(),
                               (Event->GetFitAddJet())[i].E());
          xAOD::JetFourMom_t jet4vec(jets[i].Pt(), jets[i].Eta(),
                                     jets[i].Phi(), jets[i].M());
          (Event->GetAddJets().at(i))->setJetP4(jet4vec);
        }
      }
    }
  }
  else
  {

    TLorentzVector jet1;
    TLorentzVector jet2;

    jet1.SetPtEtaPhiE(
        (Event->GetBJetOne())->pt(), (Event->GetBJetOne())->eta(),
        (Event->GetBJetOne())->phi(), (Event->GetBJetOne())->e());
    jet2.SetPtEtaPhiE(
        (Event->GetBJetTwo())->pt(), (Event->GetBJetTwo())->eta(),
        (Event->GetBJetTwo())->phi(), (Event->GetBJetTwo())->e());

    TLorentzVector jj = jet1 + jet2;

    if (i == 1)
    {
      KF1_Mbb = jj.M();
    }
    else if (i == 2)
    {

      xAOD::JetFourMom_t jet14vec(jet1.Pt(), jet1.Eta(), jet1.Phi(), jet1.M());
      xAOD::JetFourMom_t jet24vec(jet2.Pt(), jet2.Eta(), jet2.Phi(), jet2.M());

      (Event->GetBJetOne())->setJetP4(jet14vec);
      (Event->GetBJetTwo())->setJetP4(jet24vec);

      if (Event->GetAddJets().size() >= FitType::ThreeJet)
      {

        std::vector<TLorentzVector> jets(Event->GetAddJets().size());

        for (unsigned int i = 0; i < Event->GetAddJets().size(); i++)
        {
          jets[i].SetPtEtaPhiE((Event->GetAddJets().at(i))->pt(),
                               (Event->GetAddJets().at(i))->eta(),
                               (Event->GetAddJets().at(i))->phi(),
                               (Event->GetAddJets().at(i))->e());

          xAOD::JetFourMom_t jet4vec(jets[i].Pt(), jets[i].Eta(),
                                     jets[i].Phi(), jets[i].M());

          (Event->GetAddJets().at(i))->setJetP4(jet4vec);
        }
      }
    }
  }
}
