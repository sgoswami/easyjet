///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitEvent.cxx
// Source file for class KinematicFitEvent
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
/////////////////////////////////////////////////////////////////// 


// KinematicFitEvent includes
//#include <AsgTools/MessageCheck.h>
#include "KinematicFitTool/KinematicFitEvent.h"

// Constructors
////////////////

KinematicFitEvent::KinematicFitEvent() : asg::AsgTool ("KinematicFitEvent"), m_Jet_Min_Pt(0), m_BtaggingWP("")
{ 

	declareProperty( "JetMinPt", m_Jet_Min_Pt );
	declareProperty( "JetCollection", m_JetAlgo );
	declareProperty( "bTagWPDecorName", m_BtaggingWP );
}

KinematicFitEvent::~KinematicFitEvent() {

}


StatusCode KinematicFitEvent::initialize() {

	if(m_Jet_Min_Pt == 0) {
		return StatusCode::FAILURE;
	}
	if(m_BtaggingWP == "") {
		return StatusCode::FAILURE;
	}
	
	ATH_MSG_INFO("KinematicFitEvent::initialize() : KinematicFitEvent is initialized!");
  return StatusCode::SUCCESS;
}

StatusCode KinematicFitEvent::finalize() {
 

  return StatusCode::SUCCESS;
}

StatusCode KinematicFitEvent::applySelection(const xAOD::PhotonContainer& photons, const xAOD::JetContainer& jets) { 
	int n_photons = photons.size();
	int n_jets    = jets.size();                                                                                                                                                    
	if(n_photons < 2) 
	{
		return StatusCode::FAILURE;
	}
	if(n_jets < 2)
	{
		return StatusCode::FAILURE;
	}
	
	// new for modified photons
	std::vector<const xAOD::Photon*> Phots;
        for(auto phot : photons){
        Phots.push_back(phot);
	}

	
	m_photon1 = std::make_unique<xAOD::Photon>(*Phots.at(0));
	m_photon2 = std::make_unique<xAOD::Photon>(*Phots.at(1));

	std::vector<const xAOD::Jet*> b_jets;
	std::vector<const xAOD::Jet*> nb_jets;
	int nCentralJets = 0;
	static SG::AuxElement::Decorator<Bool_t> isHbb1("isHbb1");
	static SG::AuxElement::Decorator<Bool_t> isHbb2("isHbb2");
	static const SG::AuxElement::ConstAccessor<char> btagwp(m_BtaggingWP);
	for( unsigned int j=0; j<jets.size(); j++)
	{
		isHbb1(*jets.at(j)) = false;
		isHbb2(*jets.at(j)) = false;
		if( jets.at(j)->pt()*1e-3 < m_Jet_Min_Pt) continue;
		
		if (btagwp (*jets.at(j))) {

			b_jets.push_back(jets.at(j));
			
			if(j==0){
				isHbb1(*jets.at(j)) = true;
			}
			if(j==1){
				isHbb2(*jets.at(j)) = true;
			}
			
			
		}else {
			nb_jets.push_back(jets.at(j));
			if(std::abs(jets.at(j)->eta()) < 2.5) nCentralJets ++;
		}		
	}
	if(b_jets.size() < 2)
	{
		return StatusCode::FAILURE;
	}
	
	m_bjet1 = std::make_unique<xAOD::Jet>(*b_jets.at(0));
	m_bjet2 = std::make_unique<xAOD::Jet>(*b_jets.at(1));
	m_addJ = std::vector<std::unique_ptr<xAOD::Jet>>();

	int TotalCentralJets = ((b_jets.size()-2) + nCentralJets);
	
        if(TotalCentralJets >= 4){
          return StatusCode::FAILURE;
        }

	for(unsigned i = 2; i<b_jets.size(); i++)
	{
		std::unique_ptr<xAOD::Jet> add_bj = std::make_unique<xAOD::Jet>(*b_jets.at(i));
		m_addJ.push_back(std::move(add_bj));

	}
	
	for(unsigned i = 0; i<nb_jets.size(); i++)
	{
		std::unique_ptr<xAOD::Jet> add_nbj = std::make_unique<xAOD::Jet>(*nb_jets.at(i));
		m_addJ.push_back(std::move(add_nbj));
	}
	
	
  return StatusCode::SUCCESS; 
}

 xAOD::Photon* KinematicFitEvent::GetPhotonOne()
{
	return m_photon1.get();
}
 xAOD::Photon* KinematicFitEvent::GetPhotonTwo()
{
	return m_photon2.get();
}

 xAOD::Jet* KinematicFitEvent::GetBJetTwo()
{

	return m_bjet2.get();
}
 xAOD::Jet* KinematicFitEvent::GetBJetOne()
{

	return m_bjet1.get();
}

std::vector<std::unique_ptr<xAOD::Jet>>& KinematicFitEvent::GetAddJets()
{
	return m_addJ;
}

TLorentzVector KinematicFitEvent::GetFitBJetTwo()
{

	return m_bjet2_Fit;
}
TLorentzVector KinematicFitEvent::GetFitBJetOne()
{

	return m_bjet1_Fit;
}

std::vector< TLorentzVector > KinematicFitEvent::GetFitAddJet()
{
	return m_addJ_Fit;
}

void KinematicFitEvent::SetFitBJetTwo(TLorentzVector tlv)
{

	m_bjet2_Fit = tlv;
}
void KinematicFitEvent::SetFitBJetOne(TLorentzVector tlv)
{

	m_bjet1_Fit = tlv;
}

void KinematicFitEvent::SetFitAddJet(std::vector< TLorentzVector > tlv)
{
	m_addJ_Fit = tlv;
}

void KinematicFitEvent::Clear()
{
	
	m_addJ.clear();

}
