///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2019-2020 CERN for the benefit of the ATLAS collaboration
*/

// KinematicFitEvent.h
// header file for class KinematicFitEvent
// Author: BELFKIR Mohamed 
// Email : mohamed.belfkir@cern.ch
///////////////////////////////////////////////////////////////////

#ifndef KinematicFitEvent_H
#define KinematicFitEvent_H 1

#include <string.h>
#include <typeinfo>

#include <TString.h>
#include <TEnv.h>

// KinematicFitEvent includes
#include "AsgTools/AsgTool.h"
#include "AsgMessaging/AsgMessaging.h"
#include <StoreGate/ReadDecorHandle.h>
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"

class KinematicFitEvent : public asg::AsgTool {

	
public:

  /// Constructor with parameter name: 
  KinematicFitEvent();

  /// Destructor: 
  virtual            ~KinematicFitEvent(); 
  StatusCode         initialize();
  StatusCode         finalize();
  StatusCode applySelection(const xAOD::PhotonContainer& photons, const xAOD::JetContainer& jets);
  xAOD::Photon*       GetPhotonOne();
  xAOD::Photon*       GetPhotonTwo();
  xAOD::Jet*          GetBJetTwo();
  xAOD::Jet*          GetBJetOne();
  std::vector<std::unique_ptr<xAOD::Jet>>&          GetAddJets();
  TLorentzVector GetFitBJetOne();
  TLorentzVector GetFitBJetTwo();
  std::vector< TLorentzVector > GetFitAddJet();
  void SetFitBJetOne(TLorentzVector tlv);
  void SetFitBJetTwo(TLorentzVector tlv);
  void SetFitAddJet(std::vector< TLorentzVector > tlv);
  void Clear();

protected:


private:
 Double_t    m_Jet_Min_Pt;
 std::string m_BtaggingWP;
 std::string m_JetAlgo;

 std::unique_ptr<xAOD::Photon> m_photon1;
 std::unique_ptr<xAOD::Photon> m_photon2;

 std::unique_ptr<xAOD::Jet>    m_bjet1;
 std::unique_ptr<xAOD::Jet>    m_bjet2;
 std::vector<std::unique_ptr<xAOD::Jet>>        m_addJ;

 TLorentzVector m_bjet1_Fit;
 TLorentzVector m_bjet2_Fit;
 std::vector< TLorentzVector > m_addJ_Fit;

//Private members
private:

}; 

#endif //> !KinematicFitEvent_H
